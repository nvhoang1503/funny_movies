# README


## Technical required:

* Ruby version: 2.5.3

* Rails version: 5.2.4

* Database: postgresql


## How to settup the app:

* Get source code from Bitbucket: git clone git@bitbucket.org:nvhoang1503/funny_movies.git

* Database configration: 

** Rename "database.yml.example" to "database.yml" **

** Update database base on your local postgresl **
 
** Run command to create DB: bundle exec rake db:drop **

** Run command to create DB: bundle exec rake db:migrate **


## How to run the app:

** Start localhost server: rails s **

** Run unit test: bundle exec rspec **


## Check app in live:

** Heroku: https://funny-sharing.herokuapp.com/ **

** User Login (user_name/password): nvhoang1503/123456 **
