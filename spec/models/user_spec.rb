# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  user_name              :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  email                  :string
#  phone                  :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#user_name valid' do
    let(:user_name) { "test_user_name" }
    let(:email) { "test@example.com" }
    let(:password) { "123456789" }
    let(:resource) { User.create user_name: user_name, email: email , password: password}

    context 'when user_name is nil' do
      let(:user_name) { nil }
      it 'returns user_name cant be blank' do
        expect(resource.errors.messages[:user_name][0]).to eq "can't be blank"
      end
    end

    context 'when user_name is existing' do
      let(:email_1) { "test_1@example.vn" }
      let(:email_2) { "test_2@example.vn" }
      let(:resource_1) { User.create user_name: user_name, email: email_1 , password: password}
      let(:resource_2) { User.create user_name: resource_1.user_name, email: email_2 , password: password}
      
      it 'returns user_name has already been taken' do
        expect(resource_2.errors.messages[:user_name][0]).to eq "has already been taken"
      end
    end

    context 'when user_name invalid' do
      let(:user_name) { "ac!@#!@#" }
      it 'returns user_name is invalid' do
        expect(resource.errors.messages[:user_name][0]).to eq "is invalid"
      end
    end
  end

  describe '#email valid' do
    let(:user_name) { "test_user_name" }
    let(:email) { "test@example.com" }
    let(:password) { "123456789" }
    let(:resource) { User.create user_name: user_name, email: email , password: password}

    context 'when email is nil' do
      let(:email) { nil }
      it 'returns email cant be blank' do
        expect(resource.errors.messages[:email][0]).to eq "can't be blank"
      end
    end

    context 'when email is existing' do
      let(:user_name_1) { "user_name_1" }
      let(:user_name_2) { "user_name_2" }
      let(:resource_1) { User.create user_name: user_name_1, email: email , password: password}
      let(:resource_2) { User.create user_name: user_name_2, email: resource_1.email , password: password}
     
      it 'returns email has already been taken' do
        expect(resource_2.errors.messages[:email][0]).to eq "has already been taken"
      end
    end

    context 'when user_name invalid' do
      let(:email) { "ac!@#!@#" }
      it 'returns email is invalid' do
        expect(resource.errors.messages[:email][0]).to eq "is invalid"
      end
    end
  end

  describe '#password valid' do
    let(:user_name) { "test_user_name" }
    let(:email) { "test@example.com" }
    let(:password) { "123456789" }
    let(:resource) { User.create user_name: user_name, email: email , password: password}

    context 'when password is nil' do
      let(:password) { nil }
      it 'returns password cant be blank' do
        expect(resource.errors.messages[:password][0]).to eq "can't be blank"
      end
    end

    context 'when password is to short' do
      let(:password) { "1234" }
      let(:resource) { User.create user_name: user_name, email: email , password: password}    
      it 'returns password is too short' do
        expect(resource.errors.messages[:password][0]).to eq "is too short (minimum is 6 characters)"
      end
    end
  end

end
