# == Schema Information
#
# Table name: movies
#
#  id            :bigint           not null, primary key
#  user_id       :integer
#  title         :string
#  description   :string
#  like_number   :integer
#  unlike_number :integer
#  movie_url     :string
#  source_type   :integer
#  status        :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Movie, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  describe '#invalidate info' do
    let(:user_id) { nil }
    let(:title) { nil }
    let(:movie_url) { nil }
    let(:resource) { described_class.create user_id: user_id, title: title, movie_url: movie_url }
    
    context 'when user_id missing ' do
      let(:user_id) { 1000 }
      it 'return user must exist' do 
        expect(resource.errors.messages[:user][0]).to eq "must exist"
      end
    end

    context 'when title missing ' do
      let(:movie_url) { "http://test.vn" }
      it 'return title cant be blank' do 
        expect(resource.movie_url).to eq "http://test.vn"
        expect(resource.errors.messages[:title][0]).to eq "can't be blank"
      end
    end

    context 'when movie_url missing ' do
      let(:title) { "Test sharing" }
      it 'return movie_url cant be blank' do 
        expect(resource.title).to eq "Test sharing"
        expect(resource.errors.messages[:movie_url][0]).to eq "can't be blank"
      end
    end

    context 'when title && movie_url missing ' do
      it 'return title and movie_url cant be blank' do
        expect(resource.errors.messages[:title][0]).to eq "can't be blank"
        expect(resource.errors.messages[:movie_url][0]).to eq "can't be blank"
      end
    end

    context 'when movie_url has already been taken ' do
      let(:user) { create :user }
      let(:title) { "Test title" }
      let(:movie_url) { "http://google.com" }
      let(:resource_1) { described_class.create user_id: user.id, title: title, movie_url: movie_url }
      let(:resource_2) { described_class.create user_id: user.id, title: title, movie_url: movie_url }
      before do
        create :movie, user_id: user.id, title: title, movie_url: movie_url
      end
      it 'return movie_url has already been taken' do
        expect(resource_2.errors.messages[:movie_url][0]).to eq "has already been taken"
      end
    end

    context 'when movie_url is not valid ' do
      let(:user) { create :user }
      let(:title) { "Test title" }
      let(:movie_url) { "google.com" }
      let(:resource) { described_class.create user_id: user.id, title: title, movie_url: movie_url }
      
      it 'return the movie_url is not valid' do
        expect(resource.errors.messages[:movie_url][0]).to eq "The url is not valid"
      end
    end

  end

  describe '#validate info' do
    let(:user) { create :user }
    let(:title) { nil }
    let(:movie_url) { nil }
    let(:resource) { described_class.create user_id: user.id, title: title, movie_url: movie_url }

    context 'success' do 
      let(:title) { "Test sharing" }
      let(:movie_url) { "http://test.vn" }
      it 'return movie schema' do
        expect(resource.title).to eq "Test sharing"
        expect(resource.movie_url).to eq "http://test.vn"
      end
    end

  end
end
