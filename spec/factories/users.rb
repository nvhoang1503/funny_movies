# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  user_name              :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  email                  :string
#  phone                  :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

FactoryBot.define do
  factory :user do
    sequence(:user_name)  { |n| "user_name#{n}" }
    sequence(:email)      { |n| "user#{n}@example.com" }
    password              {'12345678'}
    reset_password_sent_at  { nil }
  end
end
