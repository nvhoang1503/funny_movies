# == Schema Information
#
# Table name: movies
#
#  id            :bigint           not null, primary key
#  user_id       :integer
#  title         :string
#  description   :string
#  like_number   :integer
#  unlike_number :integer
#  movie_url     :string
#  source_type   :integer
#  status        :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :movie do
    sequence(:title) { |n| "title#{n}" }
    sequence(:description) { |n| "description#{n}" }
    sequence(:movie_url) { |n| "http://movie_url#{n}.test" }
    like_number { 120 }
    unlike_number { 5 }
    status { 0 }
    source_type { 100 }

    association :user, factory: :user
  end
end
