require 'rails_helper'

RSpec.describe MoviesController, type: :controller do

  let(:user) {create :user}

  let(:valid_attributes) { 
    {
      user_id: user.id,
      title: "movie title",
      description: "movie description",
      movie_url: "http://movie_url.test"
    }
  }
  let(:invalid_attributes) {
    {
      user_id: nil,
      title: nil,
      movie_url: "movie_url.test"
    }
  }

  let(:valid_session) { {} }
  before do
    sign_in user
  end


  describe "GET movies#index" do
    context "when success" do
      let(:movie_title) { "Test movie" }
      before do
        create :movie, user_id: user.id, title: movie_title
        create_list(:movie, 10)
      end

      it "should list of movies" do
        visit movies_path

        expect(response).to be_successful
        record_num = Movie.count
        expect(record_num).to be 11
      end
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      # sign_in user
      movie = Movie.create! valid_attributes
      get :edit, params: {id: movie.to_param}, session: valid_session
      expect(response).to be_successful
    end
  end


  describe "POST #create" do
    context "with valid params" do
      it "creates a new Movie" do
        expect {
          post :create, params: {movie: valid_attributes}, session: valid_session
        }.to change(Movie, :count).by(1)
      end

      it "redirects to the created movie" do
        sign_in user
        post :create, params: {movie: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Movie.last)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested movie" do
      movie = Movie.create! valid_attributes
      expect {
        delete :destroy, params: {id: movie.to_param}, session: valid_session
      }.to change(Movie, :count).by(-1)
    end

    it "redirects to the movies list" do
      movie = Movie.create! valid_attributes
      delete :destroy, params: {id: movie.to_param}, session: valid_session
      expect(response).to redirect_to(movies_url)
    end
  end

end
