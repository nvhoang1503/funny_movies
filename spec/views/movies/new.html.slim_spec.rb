require 'rails_helper'

RSpec.describe "movies/new", type: :view do
  let(:movie) {create :movie}
  before(:each) do
    @movie = movie
  end

  it "renders new movie form" do
    render

    assert_select "form[action=?][method=?]", movie_path(@movie), "post" do
      assert_select "input[name=?]", "movie[title]"
      assert_select "input[name=?]", "movie[movie_url]"
    end
  end
end
