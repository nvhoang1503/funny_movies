require 'rails_helper'

RSpec.describe "movies/show", type: :view do
  let(:movie_title) {"Test"}
  let(:movie_1) {create :movie, title: movie_title}
  before(:each) do
    @movie = assign(:movie, movie_1)
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to render_template(partial: 'movies/_movie_card')
    expect(rendered).to match(movie_title)
    expect(rendered).to match(movie_1.description)
  end
end
