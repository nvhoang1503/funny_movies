require 'rails_helper'

RSpec.describe "movies/index", type: :view do
  let(:title_1) {nil}
  let(:title_2) {nil}
  let(:movie_1) {create :movie, title: title_1}
  let(:movie_2) {create :movie, title: title_2}

  before(:each) do
    @movies = [movie_1, movie_2]
  end

  context 'when success' do
    let(:title_1) { "Test 1"}
    let(:title_2) { "Test 2"}
    it "renders a list of movies" do
      render
      expect(rendered).to render_template(partial: 'movies/_movie_card')
      assert_select ".card-title", :text => @movies.first.title, :count => 1
    end
  end

end
