# include SourceTypeEnum
module SourceTypeEnum
  extend Enumerize

  UNKNOWN   = 0
  YOUTUBE   = 100
  VIMEO     = 200

  enumerize :source_type,
            in: {
              unknown:    UNKNOWN,
              youtube:    YOUTUBE,
              vimeo:      VIMEO
            },predicates: { prefix: true },
            scope: true,
            i18n_scope: ['enums.source_type']
end