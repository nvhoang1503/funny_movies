# == Schema Information
#
# Table name: movies
#
#  id            :bigint           not null, primary key
#  user_id       :integer
#  title         :string
#  description   :string
#  like_number   :integer
#  unlike_number :integer
#  movie_url     :string
#  source_type   :integer
#  status        :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Movie < ApplicationRecord
  include SourceTypeEnum

  belongs_to :user

  delegate :user_name, :email, :phone,
           to: :user,
           prefix: true,
           allow_nil: true

  validates :user_id, presence: true
  validates :title, presence: true
  validates :movie_url, presence: true, uniqueness: { case_sensitive: false }
  
  validates_each :movie_url do |record, attr, value|
    record.errors[:movie_url] << "The url is not valid" unless self.url_valid?(value)
  end

  def self.url_valid?(url)
    url = URI.parse(url) rescue false
    url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)
  end 

end
