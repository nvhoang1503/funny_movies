json.extract! movie, :id, :user_id, :title, :description, :like_number, :unlike_number, :movie_url, :source_type, :status, :created_at, :updated_at
json.url movie_url(movie, format: :json)
