source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.4', '>= 5.2.4.2'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'jquery-rails'
gem 'bootsnap', '>= 1.1.0', require: false
# gem 'bootstrap', '~> 4.4.1'
gem 'font-awesome-rails'
gem 'mdbootstrap-rails'
gem "slim-rails"


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # testing framework
  gem 'rspec-rails', '~> 3.5'
  gem 'rspec_junit_formatter'
  gem 'factory_bot_rails', '~> 5.0', '>= 5.0.2'
  # Guard::RSpec automatically run your specs (much like autotest)
  gem 'guard', '~> 2.14', '>= 2.14.2'
  gem 'guard-rspec', '~> 4.6.4', require: false
  gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'

end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
  gem 'spring-commands-rspec', '~> 1.0', '>= 1.0.4'
  gem 'database_cleaner', '~> 1.6', '>= 1.6.2'
  gem 'webmock', '~> 3.0.1'
  gem 'timecop', '~> 0.9.1'

  gem 'shoulda-matchers', '4.0.0.rc1'
  gem 'rails-controller-testing' # If you are using Rails 5.x
  # Generate test coverage
  gem 'simplecov'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'devise'
gem 'simple_form'
# generate db schema in ActiveRecord model
gem 'annotate', git: 'https://github.com/ctran/annotate_models.git'
# Model helpers
gem 'enumerize', '~> 2.2', '>= 2.2.1'
