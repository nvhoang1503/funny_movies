class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.integer :user_id
      t.string :title
      t.string :description
      t.integer :like_number
      t.integer :unlike_number
      t.string :movie_url
      t.integer :source_type
      t.integer :status

      t.timestamps
    end

    add_index :movies, :user_id
    add_index :movies, :source_type
    add_index :movies, :status
  end
end
