Rails.application.routes.draw do
  # devise_for :users, controllers: { sessions: 'users/sessions' }
  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions' }
  # devise_for :users

  get 'home/index'
  # root 'home#index'
  root 'movies#index'
  
  resources :movies
  # resources :movies, only: [:new, :index, :show]
end
